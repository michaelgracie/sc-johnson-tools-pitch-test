//███████████████████████████████████████████████████████████████
//       Generating Dynamic Hammer Instances MGRC 10/2016
//███████████████████████████████████████████████████████████████

//███████████████████████████████████████████████████████████████
// !IMPORTANT
// --- change the PREFIX for variables divIds, displayDivs and prefix accordingly for each pitch
var prefix = "SCJSellingTools";

var divIds = prefix + "DivIds";
var displayDivs = prefix + "DisplayDivs";
var myPitchCount = prefix + "Counter";
var myPitchImage = prefix + "Image";
//███████████████████████████████████████████████████████████████


function hammerizeDisplay(stage, $Stage, manager, deltaX, deltaY, myCurrentScale, Id) {

    $Stage = jQuery(stage);
    manager = new Hammer.Manager(stage, { multiUser: true });


    var is_iPad = navigator.userAgent.match(/iPad/i) !== null;
    var Pan = new Hammer.Pan();
    var Rotate = new Hammer.Rotate();
    var Pinch = new Hammer.Pinch();
    var Tap = new Hammer.Tap({
        taps: 1
    });
    var DoubleTap = new Hammer.Tap({
        event: 'doubletap',
        taps: 2
    });

    Rotate.recognizeWith([Pan]);
    Pinch.recognizeWith([Rotate, Pan]);
    DoubleTap.recognizeWith([Tap]);
    Tap.requireFailure([DoubleTap]);

    //recognizers
    manager.add(Pan);
    manager.add(Pinch);
    manager.add(DoubleTap);
    manager.add(Tap);

    var myProps = JSON.parse(localStorage.getItem(Id));

    if (myProps) {
        var deltaX = myProps.deltaX;
        var deltaY = myProps.deltaY;
        var coordSaved = myProps.saved;

        if (isOpen == false && coordSaved == false) {
            deltaX = deltaX; //+200
        }

        $.Velocity.hook($Stage, 'translateX', deltaX + 'px');
        $.Velocity.hook($Stage, 'translateY', deltaY + 'px');
    } else {
        var deltaX = 0;
        var deltaY = 0;
    }

    var divDeltaX = null;
    var divDeltaY = null;
    var xStart = null;
    var yStart = null;
    var xResult = null;
    var yResult = null;
    var xEnd = null;
    var yEnd = null;

    if (myCurrentScale != 1) {
        var currentScale = myCurrentScale;
        var scale = getRelativeScale(1);
        $.Velocity.hook($Stage, 'scale', scale)
    } else {
        var currentScale = 1;
    }

    function getRelativeScale(scale) {
        return scale * currentScale;
    }

    manager.on('panstart', function (e) {
        removeRedXImg();

        if (divDeltaX == null) {
            divDeltaX = e.deltaX;
            divDeltaY = e.deltaY;
        }

        if (is_iPad) {
            divDeltaX = e.deltaX; //10 replace 
            divDeltaY = e.deltaY; // 0
        }

        //  $("#scrollO2").html(divDeltaX + " "+ divDeltaY)

        var x = $("#" + Id).offset();

        osTop = x.top;
        osLeft = x.left;

        console.log(osTop, osLeft)

        // $("#scrollO").html(osTop + " "+ osLeft)

    });

    manager.on('panmove', function (e) {

        var dX = deltaX + (e.deltaX);
        var dY = deltaY + (e.deltaY);

        $.Velocity.hook($Stage, 'translateX', dX + 'px');
        $.Velocity.hook($Stage, 'translateY', dY + 'px');

    });

    manager.on('panend', function (e) {

        deltaX = deltaX + e.deltaX;
        deltaY = deltaY + e.deltaY;

        var overLap = 200;
        if (isOpen) {
            if (isCollapsed) {
                overLap = 0;
                pullDisplayBack(e, overLap, deltaX, deltaY);
            } else {
                pullDisplayBack(e, overLap, deltaX, deltaY);
            }
        } else {
            saveCoordinates(e, deltaX, deltaY)
        }


    });

    manager.on('pinchmove', function (e) {
        var scale = getRelativeScale(e.scale);
        $.Velocity.hook($Stage, 'scale', scale);
    });

    manager.on('pinchend', function (e) {
        var resize = checkThisId(e);
        if (resize) {
            var myProps = JSON.parse(localStorage.getItem(Id));
            currentScale = getRelativeScale(e.scale);
            liveScale = currentScale;
            myProps.DScale = currentScale;
            localStorage.setItem(Id, JSON.stringify(myProps));
        }
    });

    /// Remove THE DISPLAY IMAGE    
    // manager.on('doubletap', function (e) {
    //     var thisDivId = e.target.id       
    //     var idArray = JSON.parse(localStorage.getItem(divIds));
    //     for(z = 0; z < idArray.length; z++){
    //         if(thisDivId == idArray[z]){ 
    //             $("#" + thisDivId).remove();
    //             idArray.splice([z], 1);
    //         }
    //     }
    //     save_storage(divIds, JSON.stringify(idArray));
    //     localStorage.removeItem(thisDivId);
    // });


    manager.on('tap', function (e) {

        // console.log(e.target.id)  
        var id = checkThisId(e);
        if (id === false) return;

        removeRedXImg();
        var imgTag = "<img class=\"redButton\" src=\"images/DspOvrlyImgs/red_x.png\">";
        $('#' + e.target.id).append(imgTag);

        bindDeleteClickHandler();
    });


    function bindDeleteClickHandler() {
        $("img.redButton").unbind("click");
        $("img.redButton").click(function () {
            var deleteThis = $(this).parent();
            var thisDivId = deleteThis[0].id;
            var idArray = JSON.parse(localStorage.getItem(divIds));
            for (z = 0; z < idArray.length; z++) {
                if (thisDivId == idArray[z]) {
                    $("#" + thisDivId).remove();
                    idArray.splice([z], 1);
                }
            }
            save_storage(divIds, JSON.stringify(idArray));
            localStorage.removeItem(thisDivId);
        });
    }

    function removeRedXImg() {
        $('img', $('#displayArea')).each(function () {
            $(this).remove();
        });
    }

    //PULLS Display back to its original place
    function pullDisplayBack(e, overLap, deltaX, deltaY) {
        //Ids for the Displays
        var ids = JSON.parse(localStorage.getItem(displayDivs));

        if (ids == null) {
            createDisplayDivsArray();
            var ids = JSON.parse(localStorage.getItem(displayDivs));
        }

        deltaY = divDeltaN(Id, deltaY); //for displays on the canvas

        var x = e.center.x;
        var z = x - overLap;

        if (isOpen && z > 200) {
            var myDivId = e.target.id;
            var proceed = false;
            for (x = 0; x < ids.length; x++) {
                if (myDivId == ids[x]) {
                    proceed = true;
                }
            }
            if (proceed) {
                // console.log("Prceed is true");
                createDivInstance(myDivId, deltaX, deltaY);
                resetDiv();
            } else {
                saveCoordinates(e, deltaX, deltaY)
            }
        } else {

            resetDiv();
            saveCoordinates(e, deltaX, deltaY)
        }

    }

    function resetDiv() {

        var yAxis = divDeltaY;

        if (is_iPad) {
            if (gScroll != 0) {
                divDeltaX = 0
                yAxis = gScroll;
            }
        }

        $.Velocity.hook($Stage, 'translateX', divDeltaX + 'px'); // 4Displays
        $.Velocity.hook($Stage, 'translateY', yAxis + 'px'); // 4Displays
        deltaX = 0;// 4Displays
        deltaY = 0;// 4Displays

        if (is_iPad) {
            if (gScroll != 0) {
                deltaY = gScroll;
            }
        }

        // $("#imageO").html(osTop +" "+ osLeft + "   " + "gScroll"  + " " +  gScroll)

        $("#" + Id).offset({ top: osTop, left: osLeft }); // 4Displays
        osTop = 0; // 4Displays
        osLeft = 0; // 4Displays
    }

    //ENTER Margin top values for the display products
    function divDeltaN(Id, deltaY) {

        var val = Number(stripNonRealDigits($("#" + Id).css("margin-top")));

        val = val - gScroll;
        deltaY = deltaY + val;

        return deltaY;
    }


    function saveCoordinates(e, X, Y) {

        var ids = JSON.parse(localStorage.getItem(displayDivs));
        var myDivId = e.target.id;
        var proceed = false;

        for (x = 0; x < ids.length; x++) {
            if (myDivId == ids[x]) {
                proceed = true;
            }
        }

        if (!proceed) {
            var myProps = JSON.parse(localStorage.getItem(Id));

            myProps.deltaY = Y;
            myProps.deltaX = X;
            var cntr = myProps.cntr;

            if (isOpen && myProps.saved == false && myCounter != cntr) {
                myProps.saved = true;
            } else if (isOpen && myProps.saved == false) {
                // console.log("2")
            } else if (!isOpen && myProps.saved == false && myCounter != cntr) {
                myProps.saved = true;
            } else if (!isOpen && myProps.saved == false && myCounter == cntr) {
                myProps.saved = false;
            } else if (!isOpen && myProps.saved == true) {
                myProps.saved = true;
            }

            localStorage.setItem(Id, JSON.stringify(myProps));
        }
    }


    function checkThisId(e) {
        var idsArray = JSON.parse(localStorage.getItem(displayDivs));
        var myDivId = e.target.id;
        var proceed = true;
        for (x = 0; x < idsArray.length; x++) {
            if (myDivId == idsArray[x]) {
                //console.log("scaleback");
                var scale = getRelativeScale(1);
                $.Velocity.hook($Stage, 'scale', scale)
                proceed = false;
            }
        }
        if (proceed == true) {
            return true;
        } else {
            return false;
        }
    }
}


//DYNAMICALLY CREATE HAMMER OBJECTS
function createDivInstance(divId, deltaX, deltaY) {

    if (localStorage.getItem(divIds) === null) {
        var DivIds = [];
        save_storage(divIds, JSON.stringify(DivIds));
    }

    var idArray = JSON.parse(localStorage.getItem(divIds));
    if (idArray.length == 5) {
        showMessage();
        return;
    }

    var counter = getCounter(divId);
    var currentDiv = divId;

    //append pitchAbbr on divId
    divId = divId + "_" + prefix + counter;
    idArray.push(divId);
    save_storage(divIds, JSON.stringify(idArray));

    var clonedDiv = $('#' + currentDiv).clone();
    clonedDiv.attr("id", divId);
    clonedDiv.css("position", "absolute")
    clonedDiv.css("padding", "5px");
    clonedDiv.css("cursor", "move");
    clonedDiv.css("left", "20px");

    $('#displayArea').append(clonedDiv);

    saveHtmlElement(divId, currentDiv);
    createHammerInstance(divId, counter, deltaX, deltaY);

}

function getCounter(divId) {

    var a = [], count = 4;
    var deductThis = prefix.length + 2;
    var idArray = JSON.parse(localStorage.getItem(divIds));
    for (z = 0; z < idArray.length; z++) {
        var arrayIdsLength = idArray[z].length - deductThis;
        var arrayIds = idArray[z].substring(arrayIdsLength, 0)
        if (divId == arrayIds) {
            var idLength = idArray[z].length;
            var deducThisLength = idLength - 1;
            var theCount = Number(idArray[z].substring(idLength, deducThisLength));
            a.push(theCount);
        }
    }

    var missing = new Array();
    for (var i = 1; i <= count; i++) {
        if (a.indexOf(i) == -1) {
            missing.push(i);
        }
    }

    var returnThis = missing[0];
    return returnThis;
}

function saveHtmlElement(divId, currentDiv) {

    var myProps = new Object();
    var id = divId;
    divId = divId + "Temp";

    var clonedDiv = $('#' + currentDiv).clone();
    clonedDiv.attr("id", divId);
    clonedDiv.css("position", "absolute")
    clonedDiv.css("padding", "5px");
    clonedDiv.css("cursor", "move");
    clonedDiv.css("left", "20px");

    //APPEND
    $('#divTarget').html(clonedDiv);
    var toStore = $('#divTarget').html();

    myProps.divTag = toStore

    localStorage.setItem(id, JSON.stringify(myProps));
    $('#' + divId).remove();
}

function createHammerInstance(divId, counter, X, Y) {

    var myProps = JSON.parse(localStorage.getItem(divId));
    var Id = $('#' + divId).attr("id");
    var myId = document.getElementById(divId);

    var stage = 'stage' + counter;
    var $Stage = '$Stage' + counter;
    var manager = 'manager' + counter;
    var deltaX = 'deltaX' + counter
    var deltaY = 'deltaY' + counter;

    myProps.deltaX = X;
    myProps.deltaY = Y;
    myProps.stageName = 'stage' + counter;
    myProps.$StageName = '$Stage' + counter;
    myProps.managerName = 'manager' + counter;
    myProps.DScale = 1;
    myProps.saved = false;
    myProps.cntr = myCounter;

    localStorage.setItem(divId, JSON.stringify(myProps));
    stage = myId;
    myCurrentScale = 1;
    hammerizeDisplay(stage, $Stage, manager, deltaX, deltaY, myCurrentScale, Id);
}

function setCounter() {
    var c = localStorage.getItem(myPitchCount);
    if (!c) {
        myCounter = 1;
        localStorage.setItem(myPitchCount, myCounter);
    } else {
        var y = parseInt(localStorage.getItem(myPitchCount));
        myCounter = y + 1;
        localStorage.setItem(myPitchCount, myCounter);
    }
}

///////////////////////////////////////////////////////////
// LOOP THROUGH DISPLAYS CONTAINER THEN HAMMERIZE EACH DIV
function hammerizeTheDisplays() {
    var counter = 0;
    $('div', $('#pitchCanvas')).each(function () {
        var Id = $(this).attr("id");
        var myId = document.getElementById(Id);

        if (Id != 'displaysBG' && Id != 'displaysBG') {
            saveDivId(Id);
            counter++;
            var stage = 'stage' + counter;
            var $Stage = '$Stage' + counter;
            var manager = 'manager' + counter;
            var deltaX = 'deltaX' + counter
            var deltaY = 'deltaY' + counter;
            stage = myId;
            var myCurrentScale = 1;
            hammerizeDisplay(stage, $Stage, manager, deltaX, deltaY, myCurrentScale, Id);
        }
    });
}

function loadChosenProducst() { // Load displays from local storage and Hammerize it
    var ids = JSON.parse(localStorage.getItem(divIds));

    if (ids) {
        for (q = 0; q < ids.length; q++) {
            var myDivProps = JSON.parse(localStorage.getItem(ids[q]));
            var element = myDivProps.divTag;
            $('#displayArea').append(element);
            var Id = ids[q];
            replaceId(Id);

            var element = document.getElementById(Id);
            var deltaX = myDivProps.deltaX;
            var deltaY = myDivProps.deltaY;
            var stage = element;
            var $Stage = myDivProps.$StageName;
            var manager = myDivProps.managerName;
            var myCurrentScale = myDivProps.DScale;

            hammerizeDisplay(stage, $Stage, manager, deltaX, deltaY, myCurrentScale, Id);
        }
    }
}

///////////////////////////////////////////////////////////


function checkSeasonalDivIdsArray() {
    if (localStorage.getItem(displayDivs) !== null) {
        //console.log("remove displayDivs")            
        localStorage.removeItem(displayDivs);
    }
}


function saveDivId(Id) {
    if (localStorage.getItem(displayDivs) === null) {
        var myDisplayDivs = [];
        save_storage(displayDivs, JSON.stringify(myDisplayDivs));
    }
    var ids = JSON.parse(localStorage.getItem(displayDivs));
    ids.push(Id);
    save_storage(displayDivs, JSON.stringify(ids));
}


function replaceId(id) {
    var tempId = id + "Temp";
    var myId = document.getElementById(tempId);
    myId.id = id;
    return myId.id;
}


///////////////////////////////////////////////////
/////   SAVE PHOTO TO LOCAL STORAGE
//////////////////////////////////////////////////

// TAKE PHOTO FROM DEVICE CAMERA
function deviceCameraPhoto(event) {
    var photoUrl = null;
    var id = "deviceCamera";
    if (location.search == '') {
        photoUrl = "images/DspOvrlyImgs/defaultUploadImage1.jpg";
        createAndStoreDOImage(photoUrl, event, id)
    }
    else {
        var options = { sourceType: SFPresentationAPI.Camera.PictureSourceType.CAMERA };
        SFPresentationAPI.acquire_photo(options, function (photoUrl) {
            createAndStoreDOImage(photoUrl, event, id);
        }, function (e) {
            //alert("error getting photo: " + JSON.stringify(e));
            console.log("error getting photo: " + JSON.stringify(e));
        });
    }
}

// RETRIEVE PHOTO FROM CAMERA ROLL
function photoLibrary(event) {
    var photoUrl = null;
    var id = "cameraRoll";
    if (location.search == '') {
        photoUrl = "images/DspOvrlyImgs/defaultUploadImage.jpg";
        createAndStoreDOImage(photoUrl, event, id)
    }
    else {
        var options = { sourceType: SFPresentationAPI.Camera.PictureSourceType.PHOTOLIBRARY };
        SFPresentationAPI.acquire_photo(options, function (photoUrl) {
            createAndStoreDOImage(photoUrl, event, id);
        }, function (e) {
            //alert("error getting photo: " + JSON.stringify(e));
            console.log("error getting photo: " + JSON.stringify(e));
        });
    }
}

function createAndStoreDOImage(photoUrl, event, id) {

    $("#myImage").removeClass("defaultImageClass");
    $("#myImage").removeClass("centerImage");

    if (event.target.id == id) {
        document.getElementById('myImage').src = photoUrl;
        localStorage.setItem(myPitchImage, photoUrl);
        // setTimeout(function(){ checkOrientation(photoUrl); }, 10);     
        // checkOrientation(photoUrl);  
        setTimeout(function () { checkImageOrientation(); }, 100);

    } else {
        alert("Error with saving Images");
    }
}

//CHECK AND LOAD IMG FROM LOCAL STORAGE
function checkLSStoreImgData() {
    console.log(myPitchImage);

    if (localStorage.getItem(myPitchImage) !== null) {
        var storeImg = document.getElementById('myImage');
        storeImg.src = localStorage.getItem(myPitchImage);
        setTimeout(function () { checkImageOrientation(); }, 100);

        $("#myImage").removeClass("defaultImageClass");
        $("#myImage").removeClass("centerImage");
    }

}


function checkImageOrientation() {

    var myImage = $("#myImage");
    var myWidth = myImage[0].attributes[1].ownerElement.clientWidth;
    var myHeight = myImage[0].attributes[1].ownerElement.clientHeight;

    if (myWidth == 0 && myHeight == 0) {
        setTimeout(function () { checkImageOrientation(); }, 100);
        return;
    }

    if (myWidth < myHeight) {
        // $("#imageO").html("portrait");
        $("#myImage").addClass("centerImage");
    } else {
        // $("#imageO").html("Landscape");
        $("#myImage").addClass("defaultImageClass");
    }
}


function closeModal() {
    document.getElementById('myModal').style.display = "none";
}


function deletePitchHammerItems() {
    // console.log("deleting pitch hammer items")
    var idArray = JSON.parse(localStorage.getItem(divIds));

    if (idArray !== null) {
        for (z = 0; z < idArray.length; z++) {
            var removeItem = idArray[z];
            $('#' + removeItem).remove();
            localStorage.removeItem(removeItem);
        }
    }

    idArray = [];
    save_storage(divIds, JSON.stringify(idArray));

    localStorage.removeItem(myPitchImage);

    var photoUrl = null;
    photoUrl = "images/DspOvrlyImgs/ClearImage.png";
    document.getElementById('myImage').src = photoUrl;

}

function createDisplayDivsArray() {

    if (localStorage.getItem(displayDivs) === null) {
        var myDisplayDivs = [];
        save_storage(displayDivs, JSON.stringify(myDisplayDivs));
    }

    $('div', $('#pitchCanvas')).each(function () {
        var Id = $(this).attr("id");
        if (Id != 'displaysBG' && Id != 'displaysBG') {
            saveDivId(Id);
        }
    });
}

function showMessage() {
    alert("You cannot select more than 5 displays");
}

function checkCurrentSlide(currentSlide) {
    if (currentSlide == "#SCJ_DisplayOverlay1") { ///Changethis for each pitch
        checkLSStoreImgData();
        windowClick();
    } else {
        $(window).unbind('click');
    }
}

function windowClick() {
    $(window).click(function (e) {
        var targetID = e.target.id;

        if (targetID == 'pitchCanvas') {
            document.getElementById('pitchCanvas').style.display = 'none';
        }
    });
}


$(document).ready(function () {

    $(".uploadThispicture").click(function () {
        $('#myModal').css("display", "block");
    });

    $("#deviceCamera").click(function (event) {
        document.getElementById('myModal').style.display = "none";
        deviceCameraPhoto(event);
    });

    $("#cameraRoll").click(function (event) {
        document.getElementById('myModal').style.display = "none";
        photoLibrary(event);
    });


    $("#clearLS").click(function () {
        console.log("clear canvas")
        deletePitchHammerItems();
        //localStorage.clear();
    });

    var desiredWidth;
    desiredWidth = window.innerWidth;

    if (!("url" in window) && ("URL" in window)) {
        window.URL = window.URL;
    }

    isOpen = false;
    var myProducts = 0;

    $('#showProductsButton').click(function () {
        console.log("showProducts")

        document.getElementById('pitchCanvas').style.display = 'block';

        value = $('#pitchCanvas').css('left') === '200px' ? 0 : '200px';
        $('#pitchCanvas').animate({
            left: value
        });

        $('#pitchCanvas').css('left', value);
                  

        //var ids = JSON.parse(localStorage.getItem(divIds));

        if (myProducts == 0) {
            isOpen = true;
            myProducts = 1;
            $("#pitchCanvas").css("z-index", "105");

        } else {
            isOpen = false;
            myProducts = 0;
            $("#pitchCanvas").css("z-index", "1");

        }
    });

    //ScrollTop position of the Display Bar
    gScroll = $("#pitchCanvas").scrollTop();
    $("#pitchCanvas").scroll(function () {
        $("#scrollO").html($("#pitchCanvas").scrollTop());
        gScroll = $("#pitchCanvas").scrollTop();
    });

    checkSeasonalDivIdsArray();
    setCounter();
    hammerizeTheDisplays();
    loadChosenProducst();
});