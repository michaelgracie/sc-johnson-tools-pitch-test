(function (context) {
    'use strict';
    console.log('InteropService')
    var _updatedElementAttributes = {};
    var InteropService = {};

    // The JSON configuration file
    var _configData;

    // Promise that returns the cofnig data
    var _getConfigPromise;


    var _isObjectEmpty = function (obj) {

        for (var prop in obj) {

            if (obj.hasOwnProperty(prop)) {
                return false;
            }
        }

        return JSON.stringify(obj) === JSON.stringify({});
    };


    var _validateAttributes = function (objArray) {
        //todo 
    };


    var _validateImageURL = function (url) {
        return (url.match(/\.(jpeg|jpg|gif|png)$/) != null);
    };


    /**
     * Check the config file for common errors
     * @private
     */
    function _validateConfig(config) {
        var foundNames = {};

        //Check for duplicate names
        for (var i = 0; i < config.dataSources.length; i++) {
            const dataSource = config.dataSources[i];

            if (foundNames[dataSource.filteredDataName]) {
                ASM.DebugService.logWarning('The filtered data source name ' + dataSource.filteredDataName + ' is used for 2 data sources.  Filtered data source names must be unique!',
                    'datasourceDuplicateName',
                    undefined, dataSource);
            }

            foundNames[dataSource.filteredDataName] = dataSource;
        }
    }

    /**
     * Post an event to the parent client to close and navigate away from the pitch
     */
    InteropService.exitPitch = function () {
        window.parent.postMessage({pitchCloseEvent: '1'}, '*');
    };

    /**
     * Load the pitch-config.json file and parse it.
     * @returns {Promise} Resolves to the loaded config file
     */
    InteropService.getConfig = function () {
        if (_getConfigPromise) {
            return _getConfigPromise;
        } else {
            const configFileName = 'pitch-config.json';
            _getConfigPromise = new Promise(function (resolve, reject) {
                if (_configData) {
                    //Return the cachec config file
                    resolve(_configData);
                } else {

                    // 1. Create a new XMLHttpRequest object
                    var xhr = new XMLHttpRequest();

                    // 2. Configure it: GET-request for the URL /article/.../load
                    xhr.open('GET', configFileName);

                    // 3. Send the request over the network
                    xhr.send();

                    // 4. This will be called after the response is received
                    xhr.onload = function () {
                        if (xhr.status != 200) { // analyze HTTP status of the response
                            ASM.DebugService.logError('ERROR: (' + xhr.status + ') Failed to download pitch-config.json file',
                                'configFileLoadFailed',
                                undefined, e);

                            reject(e);
                        } else { // show the result
                            try {
                                _configData = JSON.parse(xhr.response);

                                _validateConfig(_configData);

                                resolve(_configData);
                            } catch (e) {
                                ASM.DebugService.logError('ERROR deserializing JSON config file: (' + e.message + ')',
                                    'configFileJSONError',
                                    undefined, e);

                                reject(e);
                            }

                        }
                    };

                    xhr.onerror = function (e) {
                        ASM.DebugService.logError('Could not download pitch-config.json file',
                            'configFileLoadFailed',
                            undefined, e);

                        reject(e);
                    };
                }
            });


            return _getConfigPromise;
        }
    };

    /**
     * Returns true if we are running in an iFrame within the mobile client or in the admin editor
     */
    InteropService.isRunningInApp = function (){
        // This just detects if we are in an iFrame or not.  may want to check if this iFrame is actually our
        // client and editor, but currently we just assume it is.
        return window.parent.length > 0;
    };


    InteropService.updateElementAttributes = function (elementName, eleAttributeList) {

        if ((ASM.db).hasOwnProperty(elementName)) {

            //check if attribute values are updated

            var initAttributeValues = JSON.stringify(Object.entries(eleAttributeList).sort());
            var updatedAttributeValues = JSON.stringify(Object.entries((ASM.db)[elementName]).sort());
            if (initAttributeValues === updatedAttributeValues) {
                return;
            }

        } else {

            //new editable element's attributes are updated

            if (_isObjectEmpty(eleAttributeList)) {
                return;
            }
        }


        _updatedElementAttributes[elementName] = eleAttributeList;
        window.parent.postMessage(_updatedElementAttributes, '*');
    };



    window.addEventListener('DOMContentLoaded', function (event) {
        //ASM.PitchBookRenderService.render();
    });


    if (InteropService.isRunningInApp()) { 
        window.addEventListener('message', function (event) { 

            if(event.data.data){
                ASM.db=event.data.data;//on every load/update call we will need to update ASM.db as it recive the latest applied changes from parent
                if(event.data.mode==='update'){
                    ASM.PitchBookRenderService.render(renderMode);//re render in case of update mode
                } 
            }
            if (event.data.message === 'dataSourcesInitialization') { 
                var renderMode = event.data.mode;
                //console.log(`Received ${event.data} from ${event.origin}`);
                ASM.DataSourcesService._initializeData(event.data.data); //tobe removed later
                ASM.PitchBookRenderService.render(renderMode);
            } else if (event.data.message === 'dataSourcesInitializationError') {
                ASM.DebugService.logError('Failed to initialize data sources',
                    'dataSourcesInitializationError',
                    undefined, event.data.error);
            }


        });
    } else {
        // setTimeout is wait in case the datasources service has not been loaded yet
        ASM.ready.then(function(){
            //Initialize debug data since we have no parent
            ASM.DataSourcesService._initializeData();
        });

    }





    context.InteropService = InteropService;



})(ASM);